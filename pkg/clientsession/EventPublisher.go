package clientsession

type EventPublisher interface {
	SendMonsterEvent(event interface{}) error
}

type EventType string

const (
	MonsterEvent EventType = "Monster"
)
