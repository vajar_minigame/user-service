package data

import (
	"time"
)

type UserComp struct {
	Username   string    `protobuf:"bytes,2,opt,name=username,proto3" json:"username,omitempty"`
	LastUpdate time.Time `protobuf:"bytes,3,opt,name=last_update,json=lastUpdate,proto3,stdtime" json:"last_update,omitempty" db:"last_update"`
	IsTest     bool
}
