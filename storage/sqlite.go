package storage

import (
	"context"
	"log"

	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/vajar_minigame/user-service/config"
	"gitlab.com/vajar_minigame/user-service/ent"
)

type SqliteDB struct {
	client *ent.Client
}

func NewSqliteDB() (UserDB, error) {

	client, err := ent.Open("sqlite3", "file:ents.db?&cache=shared&_fk=1")
	if err != nil {
		log.Fatal(err)
	}

	if err := client.Schema.Create(context.Background()); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}

	userRepo := NewUserDB(client)

	return userRepo, nil
}

//NewDatabases creates all databases in one go
func NewDatabases(config *config.Config) UserDB {
	userDB, _ := NewSqliteDB()

	return userDB

}
