package controller

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/vajar_minigame/user-service/pkg/clientsession"
	"gitlab.com/vajar_minigame/user-service/pkg/data/usermodel"

	"gitlab.com/vajar_minigame/user-service/config"
	"gitlab.com/vajar_minigame/user-service/storage"
)

//UserService dependencies
type UserController struct {
	db     storage.UserDB
	config *config.Config
	client clientsession.EventPublisher
}

// NewUserService creates new userservice
func NewUserController(config *config.Config, userDB storage.UserDB, client clientsession.EventPublisher) *UserController {

	return &UserController{db: userDB, client: client, config: config}

}

func (s *UserController) AddTestUser(w http.ResponseWriter, r *http.Request) {

	ctx := context.Background()
	userDto := usermodel.UserDto{Username: "testUser", IsTest: true}
	createdUser, err := s.db.AddUser(ctx, userDto)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		logrus.Error("Failed update %w", err)
		return
	}

	logrus.Infof("User %v was added", createdUser.ID)

	w.WriteHeader(http.StatusCreated)

}
func (s *UserController) AddUser(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	userDto := usermodel.UserDto{}
	body, err := io.ReadAll(r.Body)
	if err != nil {
		logrus.Error("Could not read body %w", err)
		return
	}
	err = json.Unmarshal(body, &userDto)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		logrus.Error("Could not unmarshal body %w", err)
		return
	}
	createdUser, err := s.db.AddUser(ctx, userDto)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		logrus.Error("Failed update %w", err)
		return
	}

	logrus.Infof("User %v was added", createdUser.ID)

	w.WriteHeader(http.StatusCreated)
	w.Header().Set("Content-Type", "application/json")
	createdUserByte, err := json.Marshal(createdUser)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		logrus.Error("Failed update %w", err)
		return
	}
	w.Write(createdUserByte)
}

//GetUserByID returns the user with the id
func (s *UserController) GetUserByID(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	ctx := context.Background()
	vars := mux.Vars(r)
	idStr, ok := vars["id"]

	if ok == false {
		w.WriteHeader(http.StatusBadRequest)
		logrus.Error("id variable not there")
		fmt.Fprintf(w, "Error")
		return
	}

	id, err := strconv.Atoi(idStr)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		logrus.Error(err)
		fmt.Fprintf(w, "Ooops")
		return
	}

	user, err := s.db.GetUserByID(ctx, id)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		logrus.Error(err)
		fmt.Fprintf(w, "Ooops")
		return
	}
	userSerialized, err := json.Marshal(user)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		logrus.Error(err)
		fmt.Fprintf(w, "Ooops")
		return
	}

	w.Write(userSerialized)

}
