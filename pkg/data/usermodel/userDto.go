package usermodel

import (
	"time"

	"gitlab.com/vajar_minigame/user-service/ent"
)

type UserDto struct {
	ID         int    `json:"id,omitempty" db:"id"`
	Username   string `json:"username,omitempty" db:"username"`
	LastUpdate time.Time
	IsTest     bool `json:"isTest,omitempty" db:"isTest"`
}

func NewUserFromEnt(userEnt *ent.User) UserDto {
	return UserDto{ID: userEnt.ID, Username: userEnt.Name, LastUpdate: userEnt.LastUpdate, IsTest: userEnt.IsTest}
}
