package storage

import (
	"context"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/vajar_minigame/user-service/ent"
	entuser "gitlab.com/vajar_minigame/user-service/ent/user"
	"gitlab.com/vajar_minigame/user-service/pkg/data/usermodel"
)

// NewUserDB creates a usable user db
func NewUserDB(client *ent.Client) *UserMock {

	return &UserMock{client: client}
}

type UserMock struct {
	client *ent.Client
}

//GetAllUsers returns all users that are listed in the DB
func (db *UserMock) GetAllUsers(ctx context.Context) ([]usermodel.UserDto, error) {

	users := []usermodel.UserDto{}

	return users, nil

}

// GetUserByID .
func (db *UserMock) GetUserByID(ctx context.Context, id int) (usermodel.UserDto, error) {

	user, err := db.client.User.Get(ctx, id)
	if err != nil {
		logrus.Errorf("Error getting userdto %w", err)
		return usermodel.UserDto{}, err
	}

	currUser := usermodel.NewUserFromEnt(user)

	return currUser, nil

}

// GetUserByName .
func (db *UserMock) GetUserByUsername(ctx context.Context, username string) (usermodel.UserDto, error) {

	user, err := db.client.User.Query().Where(entuser.Name("username")).First(ctx)
	if err != nil {
		logrus.Errorf("the specified user couldn't be found %w", err)
		return usermodel.UserDto{}, err
	}
	currUser := usermodel.NewUserFromEnt(user)
	return currUser, nil

}

//AddUser add a user
func (db *UserMock) AddUser(ctx context.Context, u usermodel.UserDto) (usermodel.UserDto, error) {

	storedUser, err := db.client.User.Create().
		SetIsTest(u.IsTest).
		SetLastUpdate(time.Now().UTC()).
		SetName(u.Username).Save(ctx)

	if err != nil {
		logrus.Errorf("Can't add user %w")
		return usermodel.UserDto{}, err
	}
	currUser := usermodel.NewUserFromEnt(storedUser)
	return currUser, nil

}
