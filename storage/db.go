package storage

import (
	"context"
	"fmt"

	"gitlab.com/vajar_minigame/user-service/pkg/data/usermodel"

	"github.com/sirupsen/logrus"
)

// DB
type UserDB interface {
	AddUser(ctx context.Context, user usermodel.UserDto) (usermodel.UserDto, error)
	GetAllUsers(ctx context.Context) ([]usermodel.UserDto, error)

	GetUserByID(ctx context.Context, id int) (usermodel.UserDto, error)
	GetUserByUsername(ctx context.Context, username string) (usermodel.UserDto, error)
}

func BootstrapDBs(userDB UserDB) {

	bootstrapUserDB(userDB)

}

func bootstrapUserDB(m UserDB) int {

	// user
	user := usermodel.UserDto{ID: 0, Username: "vajar"}
	u, err := m.AddUser(context.Background(), user)

	if err != nil {
		logrus.Fatalf("error bootstraping db: %v", err)
	}

	fmt.Printf("Added user with id: %d\n", u.ID)

	return u.ID

}
